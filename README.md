## How to build project

1. Clone the repository
1. Go to the root of the project
1. Run gradle task `installDist`
1. Run executable in `build/install/stepik-top-courses/bin/`

### Example for linux
1. `git clone https://gitlab.com/grepcake/stepik-top-courses`
1. `cd stepik-top-courses`
1. `chmod +x gradlew`
1. `./gradlew installDist`
1. `chmod +x build/install/stepik-top-courses/bin/stepik-top-courses`
1. `build/install/stepik-top-courses/bin/stepik-top-courses 10`
