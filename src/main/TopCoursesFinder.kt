import com.google.gson.GsonBuilder
import org.apache.http.HttpStatus
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils
import java.io.IOException

const val usageMessage = "Usage: [NUMBER]"
const val stepikUrl = "https://stepik.org"
const val stepikApiUrl = "$stepikUrl/api"
val httpClient = HttpClients.createDefault()!!

fun main(args: Array<String>) {
    if (args.size != 1) {
        System.err.println("Invalid usage:\n$usageMessage")
        return
    }
    val n = args[0].toIntOrNull()
    if (n == null) {
        System.err.println("Provided argument is not a number: ${args[0]}")
        return
    }
    if (n < 0) {
        System.err.println("Number cannot be negative: $n")
        return
    }

    try {
        val courses = getCourses().asSequence().sortedByDescending { it.learnersCount }.take(n)
        println(prettify(courses))
    } catch (e: IOException) {
        System.err.println("Failed to receive course lists: ${e.message}")
    }

}

fun getCourses(): List<Course> {
    var pageNumber = 0
    val result = mutableListOf<Course>()
    while (addCoursesFromPage(result, pageNumber)) {
        ++pageNumber
    }
    return result
}

fun addCoursesFromPage(destination: MutableList<Course>, pageNumber: Int): Boolean {
    val request = HttpGet("$stepikApiUrl/courses?page=${pageNumber + 1}")
    val response = httpClient.execute(request)
    val responseString = EntityUtils.toString(response.entity)
    val statusLine = response.statusLine
    if (statusLine.statusCode != HttpStatus.SC_OK) {
        throw IOException("Failed to perform request: $responseString")
    }
    val gson = GsonBuilder().create()
    val coursesContainer = gson.fromJson(responseString, CoursesContainer::class.java)
    destination.addAll(coursesContainer.courses)
    return coursesContainer.meta["has_next"] == true
}

const val separator = "\n-----------\n"

fun prettify(courses: Sequence<Course>) = courses
        .mapIndexed { index, course -> format(index, course) }
        .joinToString(separator)

private fun format(index: Int, course: Course): String {
    return "#${index + 1} ${course.title} ($stepikUrl/course/${course.id})\n" +
            "${course.description}\n" +
            "learners count: ${course.learnersCount}"
}
